﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SoloLearn.ChatAPI.Entities.Models
{
    [Table("account")]
    public class Account
    {
        [Key]
        public int AccountId { get; set; }

        [Required(ErrorMessage = "DateCreated is required")]
        public DateTime DateTimeCreated { get; set; }

        [Required(ErrorMessage = "Username is required")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }

        [NotMapped]
        public string AuthenticationData { get; set; }

        [Required(ErrorMessage = "First name is required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }
    }
}
