﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SoloLearn.ChatAPI.Entities.Models
{
    [Table("message")]
    public class Message
    {
        [Key]
        public int MessageId { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "Message should not exceed 200 charachters")]
        public string Text { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        [ForeignKey("Account")]
        public int AccountId { get; set; }

        public Account Account { get; set; }

        [ForeignKey("Chatroom")]
        public int ChatroomId { get; set; }

        public Chatroom Chatroom { get; set; }
    }
}
