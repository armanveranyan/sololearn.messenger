﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SoloLearn.ChatAPI.Entities.Models
{
    [Table("chatroom")]
    public class Chatroom
    {
        [Key]
        public int ChatroomId { get; set; }

        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; }

        [Required(ErrorMessage = "DateCreated is required")]
        public DateTime DateTimeCreated { get; set; }

    }
}
