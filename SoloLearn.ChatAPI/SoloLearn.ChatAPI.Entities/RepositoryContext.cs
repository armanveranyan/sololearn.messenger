﻿using Microsoft.EntityFrameworkCore;
using SoloLearn.ChatAPI.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoloLearn.ChatAPI.Entities
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options)
          : base(options)
        {
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Chatroom> Chatrooms { get; set; }
        public DbSet<Message> Messages { get; set; }

        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            foreach (var relationship in modelbuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(modelbuilder);
        }
    }
}
