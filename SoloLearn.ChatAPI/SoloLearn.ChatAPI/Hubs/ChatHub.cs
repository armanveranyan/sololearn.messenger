﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using SoloLearn.ChatAPI.Contracts;
using SoloLearn.ChatAPI.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoloLearn.ChatAPI.Hubs
{
    public interface IChatHub
    {
        Task Connect(int chatId);
        Task Disconnect(int chatId);
        Task SendMessage(string message, int chatId);
    }

    public class ChatHub : Hub, IChatHub
    {
        private IRepositoryWrapper _repoWrapper;

        public ChatHub(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        [Authorize]
        public async Task Connect(int chatId)
        {
            var chatroom = _repoWrapper.Chatroom.FindOneByCondition(c => c.ChatroomId == chatId);

            if (chatroom != null)
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, chatId.ToString());
                var messages = _repoWrapper.Message.FindByCondition(m => m.AccountId == 1);
                await Clients.Group(chatId.ToString()).SendAsync("Connected", Context.User.Identity.Name, messages);
            }
        }

        [Authorize]
        public async Task Disconnect(int chatId)
        {
            if (_repoWrapper.Chatroom.FindOneByCondition(c => c.ChatroomId == chatId) != null)
            {
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, chatId.ToString());
            }
        }

        [Authorize]
        public async Task SendMessage(string message, int ChatroomId)
        {
            var chatroom = _repoWrapper.Chatroom.FindOneByCondition(c => c.ChatroomId == ChatroomId);
            var user = _repoWrapper.Account.FindOneByCondition(c => c.Username == Context.User.Identity.Name);

            if (chatroom != null)
            {
                var msg = new Entities.Models.Message
                {
                    AccountId = user.AccountId,
                    ChatroomId = ChatroomId,
                    Text = message,
                    CreatedDate = DateTime.UtcNow
                };

                _repoWrapper.Message.Create(msg);

                _repoWrapper.Message.Save();

                await Clients.Group(ChatroomId.ToString()).SendAsync("ReceiveMessage", Context.User.Identity.Name, msg);
            }
        }
    }
}
