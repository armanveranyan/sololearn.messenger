﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.AspNet.SignalR.Hubs;
using SoloLearn.ChatAPI.Hubs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Http.Connections;
using Owin;
using Microsoft.Owin;
using PathString = Microsoft.AspNetCore.Http.PathString;
using Microsoft.Owin.Cors;
using SoloLearn.ChatAPI.Entities;
using Microsoft.EntityFrameworkCore;
using SoloLearn.ChatAPI.Extensions;
using SoloLearn.ChatAPI.Contracts;
using SoloLearn.ChatAPI.Repository;

[assembly: OwinStartup(typeof(SoloLearn.ChatAPI.Startup))]
namespace SoloLearn.ChatAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });

            services.ConfigureDbContext(Configuration);

            services.ConfigureAuthentication();

            services.AddMvc();

            services.ConfigureCors();

            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseCors("CorsPolicy");

            app.UseAuthentication();

            app.UseMvc();

            app.UseSignalR(routes =>
            {
                routes.MapHub<ChatHub>("/api/chat");
            });
        }
    }
}
