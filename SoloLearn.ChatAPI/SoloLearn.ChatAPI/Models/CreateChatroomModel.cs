﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SoloLearn.ChatAPI.Models
{
    public class CreateChatroomModel
    {
        [Required]
        public string ChatName { get; set; }
    }
}
