﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SoloLearn.ChatAPI.Entities.Models;
using SoloLearn.ChatAPI.Services.Contracts;
using SoloLearn.ChatAPI.Models;
using SoloLearn.ChatAPI.Contracts;

namespace SoloLearn.ChatAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private IAccountService _accountService;
        private IRepositoryWrapper _repoWrapper;

        public AccountController(IRepositoryWrapper repoWrapper, IAccountService accountService)
        {
            _accountService = accountService;
            _repoWrapper = repoWrapper;
        }

        [Authorize]
        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody]AuthenticateModel userParam)
        {
            var user = await _accountService.Login(userParam.Username, userParam.Password);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(new { AccountId = user.AccountId, Username = user.Username, AuthenticationData = user.AuthenticationData });
        }

        [Authorize]
        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]RegisterModel userParam)
        {
            var user = await _accountService.CreateUser(userParam.Username, userParam.Password, userParam.FirstName, userParam.LastName);

            if (user == null)
                return BadRequest(new { message = "User already exists in database" });

            return Ok(new { AccountId = user.AccountId, Username = user.Username, AuthenticationData = user.AuthenticationData });
        }

        [Authorize]
        [HttpGet("all")]
        public async Task<IActionResult> GetAllUsers()
        {
            var user = await _accountService.GetAll();

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user);
        }
    }
}