﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SoloLearn.ChatAPI.Contracts;
using SoloLearn.ChatAPI.Models;

namespace SoloLearn.ChatAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ChatroomController : ControllerBase
    {
        private IRepositoryWrapper _repoWrapper;

        public ChatroomController(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        [Authorize]
        [AllowAnonymous]
        [HttpGet("all")]
        public async Task<JsonResult> GetAllChatrooms()
        {
            return await Task.Factory.StartNew(() =>
            {
                var res = _repoWrapper.Chatroom.FindAll();
                return new JsonResult(res);
            });
        }

        [Authorize]
        [AllowAnonymous]
        [HttpPost("Create")]
        public async Task<JsonResult> Create([FromBody]CreateChatroomModel chat)
        {
            return await Task.Factory.StartNew(() =>
            {
                _repoWrapper.Chatroom.Create(new Entities.Models.Chatroom
                {
                    Title = chat.ChatName,
                    DateTimeCreated = DateTime.UtcNow,
                });

                _repoWrapper.Chatroom.Save();

                return new JsonResult(_repoWrapper.Chatroom.FindAll());
            });
        }

        [Authorize]
        [AllowAnonymous]
        [HttpDelete("delete/{id:int}")]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            return await Task.Factory.StartNew(() =>
            {
                var chatroom = _repoWrapper.Chatroom.FindOneByCondition(c => c.ChatroomId == id);

                if (chatroom != null)
                {
                    _repoWrapper.Chatroom.Delete(chatroom);
                    _repoWrapper.Chatroom.Save();
                }

                return Ok(_repoWrapper.Chatroom.FindAll());
            });
        }
    }
}
