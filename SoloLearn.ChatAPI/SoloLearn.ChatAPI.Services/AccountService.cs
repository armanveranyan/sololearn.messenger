﻿using SoloLearn.ChatAPI.Contracts;
using SoloLearn.ChatAPI.Entities.Models;
using SoloLearn.ChatAPI.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SoloLearn.ChatAPI.Services
{
    public class AccountService : IAccountService
    {
        private IRepositoryWrapper _repoWrapper;

        public AccountService(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        public async Task<Account> Login(string username, string password)
        {
            password = Convert.ToBase64String(Encoding.UTF8.GetBytes(password));
            var user = await Task.Run(() => _repoWrapper.Account.FindOneByCondition(x => x.Username == username && x.Password == password));

            // return null if user not found
            if (user == null)
                return null;

            user.AuthenticationData = Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Join(":", user.Username, user.Password)));

            return user;
        }

        public async Task<Account> Authenticate(string username, string password)
        {
            var user = await Task.Run(() => _repoWrapper.Account.FindOneByCondition(x => x.Username == username && x.Password == password));

            // return null if user not found
            if (user == null)
                return null;

            return user;
        }

        public async  Task<Account> CreateUser(string username, string password, string firstName, string lastName)
        {
            var user = await Task.Run(()=> 
            {
                return _repoWrapper.Account.FindOneByCondition(u => u.Username == username);
            });

            if (user == null)
            {
                user = new Account
                {
                    DateTimeCreated = DateTime.UtcNow,
                    Username = username,
                    // TODO Real encoding algorithm should be implemented
                    Password = Convert.ToBase64String(Encoding.UTF8.GetBytes(password)),
                    FirstName = firstName,
                    LastName = lastName,
                };

                _repoWrapper.Account.Create(user);
                _repoWrapper.Account.Save();
                user.AuthenticationData = Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Join(":", user.Username, user.Password)));

                return user;
            }

            return null;
        }

        public async Task<IEnumerable<Account>> GetAll()
        {
            var users = await Task.Run(() => _repoWrapper.Account.FindAll());

            return users;
        }
    }
}
