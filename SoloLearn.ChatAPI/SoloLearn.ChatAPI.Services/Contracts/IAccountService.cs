﻿using SoloLearn.ChatAPI.Entities.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SoloLearn.ChatAPI.Services.Contracts
{
    public interface IAccountService
    {
        Task<Account> Login(string username, string password);
        Task<Account> Authenticate(string username, string password);
        Task<IEnumerable<Account>> GetAll();
        Task<Account> CreateUser(string username, string password, string firstName, string lastName);
    }
}
