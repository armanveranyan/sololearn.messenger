﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SoloLearn.ChatAPI.Utilities.Extensions
{
    public class StartupExtensions
    {
        public void ConfigureCors(this IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<RepositoryContext>(options => options.UseSqlServer(Configuration.GetConnectionString("SoloDatabase")));


            services.AddCors(option => {
                option.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowAnyOrigin()
                    .AllowCredentials());
            });

            services.AddSignalR();
        }

    }
}
