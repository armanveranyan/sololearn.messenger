﻿using SoloLearn.ChatAPI.Contracts;
using SoloLearn.ChatAPI.Entities;
using SoloLearn.ChatAPI.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoloLearn.ChatAPI.Repository
{
    public class ChatroomRepository: RepositoryBase<Chatroom>, IChatroomRepository
    {
        public ChatroomRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
