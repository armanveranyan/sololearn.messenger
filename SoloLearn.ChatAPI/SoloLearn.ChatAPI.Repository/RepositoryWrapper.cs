﻿using SoloLearn.ChatAPI.Contracts;
using SoloLearn.ChatAPI.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoloLearn.ChatAPI.Repository
{
    public class RepositoryWrapper: IRepositoryWrapper
    {
        private RepositoryContext _repoContext;
        private IChatroomRepository _chatroom;
        private IAccountRepository _account;
        private IMessageRepository _message;

        public IChatroomRepository Chatroom
        {
            get
            {
                if (_chatroom == null)
                {
                    _chatroom = new ChatroomRepository(_repoContext);
                }

                return _chatroom;
            }
        }

        public IAccountRepository Account
        {
            get
            {
                if (_account == null)
                {
                    _account = new AccountRepository(_repoContext);
                }

                return _account;
            }
        }

        public IMessageRepository Message
        {
            get
            {
                if (_message == null)
                {
                    _message = new MessageRepository(_repoContext);
                }

                return _message;
            }
        }

        public RepositoryWrapper(RepositoryContext repositoryContext)
        {
            _repoContext = repositoryContext;

            repositoryContext.Database.EnsureCreated();
        }
    }
}

