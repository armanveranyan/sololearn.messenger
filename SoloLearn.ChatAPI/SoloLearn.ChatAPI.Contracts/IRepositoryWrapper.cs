﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SoloLearn.ChatAPI.Contracts
{
    public interface IRepositoryWrapper
    {
        IChatroomRepository Chatroom { get; }
        IAccountRepository Account { get; }
        IMessageRepository Message { get; }
    }
}
