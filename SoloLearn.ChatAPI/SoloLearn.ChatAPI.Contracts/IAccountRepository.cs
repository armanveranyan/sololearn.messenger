﻿using SoloLearn.ChatAPI.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoloLearn.ChatAPI.Contracts
{
    public interface IAccountRepository : IRepositoryBase<Account>
    {
    }
}
