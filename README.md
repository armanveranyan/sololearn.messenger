## Technologies used
1. .NET Core 2.1 - Web API
2.  EFCore 
2.  React - UI
3.  ASP.NET SignalR - .NET Core and React libraries 

## Prerequisites 
1. .NET Core 2.1
2.  MSSQL Server - Code first approach is used, Database will be created runtime 
3. .NET Core Runtime 2.2 (Optional/for hosting inside IIS Express)
4.  NPM >= 5.6.0

## Configurations
1. Connection string is required in appsettings.json file, it uses local server by default.
2. Port 5000 is used for self hosting of Web Api, change it in case other applications use that port on your local machine (see 2 next steps).
3. Web API - SoloLearn.ChatAPI.Hosting.Program.cs line 14    
4. React App - sololearn-messenger/src/config.js 

# How to run
##  Hosting the backend (Kestrel server)
1. Open SoloLearn.ChatAPI.sln, set SoloLearn.ChatAPI.Hosting project as startup (if not by default)
2. Ctrl+F5 :)
3. The first request (most probably Sign Up) to the backend could be slower, because database gets created at that time


## Running the UI
1. Open CMD, CD to solo-messenger folder
2. Run npm install command
3. Run npm start command

