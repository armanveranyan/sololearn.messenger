import React, { Component } from "react";
import moment from "moment";

class Message extends Component {
  render() {
    return (
      <li className="ChatLog__entry">
        <img
          className="ChatLog__avatar"
          src={require("../images/user_logo.png")}
        />
        <div
          title={moment(this.props.message.createdDate).format("HH:mm:ss")}
          className="ChatLog__message"
        >
          {this.props.message.text}
          <div className="ChatLog__timestamp">
            {this.props.message.sender}(
            {moment(this.props.message.createdDate).format("HH:mm:ss")})
          </div>
        </div>
      </li>
    );
  }
}

export default Message;
