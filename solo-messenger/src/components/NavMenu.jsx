import React, { Component } from "react";

export class NavMenu extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <nav className="navbar navbar-dark fixed-top bg-primary flex-md-nowrap p-0 shadow">
          <a className="navbar-brand col-sm-3 col-md-2 mr-0" href="#">
          Solo Messenger ({this.props.User.username})
          </a>
          <ul className="navbar-nav px-3">
            <li className="nav-item text-nowrap">
              <a className="nav-link" href="/login">
                Sign out
              </a>
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}

export default NavMenu;
