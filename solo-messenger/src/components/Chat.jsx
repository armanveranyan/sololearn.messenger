import React, { Component } from "react";
import ReactDOM from "react-dom";
import Message from "./Message";
import { ChatService } from "../services/ChatWSService";
import { getAuthenticationData } from "../Helpers/auth-header";

export class Chat extends Component {
  constructor(props) {
    super(props);

    this.state = {
      messages: [],
      currentMessage: ""
    };

    this.handleMessageRef = this.handleMessageRef.bind(this);
    this.handleMessageChange = this.handleMessageChange.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
    this.onConnect = this.onConnect.bind(this);
    this.receivedMessage = this.receivedMessage.bind(this);
    this.scrollToBottom = this.scrollToBottom.bind(this);
    this.handlePanelRef = this.handlePanelRef.bind(this);
  }

  render() {
    if (this.props.chatId == 0) {
      return (
        <div className="chat col-md-10">
          <p>Connect to one of the available chats.</p>
        </div>
      );
    }

    return (
      <div className="row">
        <div className="col-md-12 message-view">
          <div className="panel-body panel-chat" ref={this.handlePanelRef}>
            <ul>
              {this.state.messages.map(message => (
                <Message key={message.messageId} message={message} />
              ))}
            </ul>
          </div>
        </div>
        <div className="col-md-12">
          <form className="chat-form" onSubmit={e => this.onSubmit(e)}>
            <div className="input-group input-group-lg">
              <input
                type="text"
                value={this.state.currentMessage}
                className="form-control"
                ref={this.handleMessageRef}
                id="msg"
                autoComplete="off"
                onChange={this.handleMessageChange}
                placeholder="Your message"
              />
              <div className="input-group-append">
                <button type="submit" className="btn btn-primary btn-sm">
                  Send
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }

  msg;
  panel;

  handlePanelRef(div) {
    console.log("222222222222", this.refs, this.panel);
    this.panel = div;
  }

  handleMessageRef(input) {
    this.msg = input;
  }

  handleMessageChange(event) {
    this.setState({ currentMessage: event.target.value });
  }

  sendMessage() {
    let currentMessage = this.state.currentMessage;
    if (currentMessage.length === 0) {
      return;
    }

    ChatService.SendMessage(this.props.chatId, currentMessage);
  }

  onSubmit(event) {
    event.preventDefault();
    this.sendMessage();
  }

  _chatId;
  _registered;
  componentDidUpdate = () => {
    if (this.props.chatId != 0 && this._chatId != this.props.chatId) {
      this._chatId = this.props.chatId;

      ChatService.Connect(this.props.chatId, this.onConnect);
      if (!this._registered) {
        ChatService.RegisterCallbacks(this.receivedMessage);
        this._registered = true;
      }
    }
    this.scrollToBottom();
  };

  scrollToBottom = () => {
    // Tried anything, nothing worked :D
    window.scrollTo(0, 9999999999);
  };

  receivedMessage(message, user) {
    let messages = this.state.messages;
    message.sender = user;
    console.log(message);
    messages.push(message);
    this.setState({ messages: messages, currentMessage: "" });
    this.scrollToBottom();
    this.msg.focus();
  }

  onConnect(chatMessages) {
    console.log(chatMessages);
    this.setState({
      messages: chatMessages
    });
  }
}

export default Chat;
