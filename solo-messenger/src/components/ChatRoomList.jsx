import React, { Component } from "react";
import ReactDOM from "react-dom";
import ChatRoom from "./ChatRoom";
import Chat from "./Chat";
import ChatroomService from "../services/ChatroomService";
import {
  HubConnection,
  HubConnectionBuilder,
  HttpTransportType,
  JsonHubProtocol,
  LogLevel
} from "@aspnet/signalr";

export class ChatRoomList extends Component {
  constructor() {
    super();

    this.state = {
      chatrooms: [],
      currentChatroom: 0
    };

    this.SelectRoom = this.SelectRoom.bind(this);
    this.DeleteChatroom = this.DeleteChatroom.bind(this);
    this.CreateANewChatroom = this.CreateANewChatroom.bind(this);
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <nav className="col-md-2 d-none d-md-block bg-light sidebar shadow">
            <div className="sidebar-sticky">
              <a className="bd-toc-link">Available Chatrooms</a>
              <ul className="nav flex-column">
                {this.state.chatrooms.map(chatroom => (
                  <ChatRoom
                    key={chatroom.chatroomId}
                    chatroom={chatroom}
                    selectRoom={this.SelectRoom}
                    DeleteChatRoom={this.DeleteChatroom}
                  />
                ))}
              </ul>
              <a href="#" onClick={e => this.CreateANewChatroom(e)}>
                Create a new chatroom...
              </a>
            </div>
          </nav>
          <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
            <Chat chatId={this.state.currentChatroom} />
          </main>
        </div>
      </div>
    );
  }

  CreateANewChatroom(event) {
    event.preventDefault();

    var chatName = prompt("Please enter a name for the chat", "");

    let that = this;

    if (chatName) {
      ChatroomService.CreateChatroom(chatName).then(chatrooms => {
        if (chatrooms) {
          that.setState({
            chatrooms: chatrooms,
            currentChatroom:
              that.state.currentChatroom == 0
                ? chatrooms[0].chatroomId
                : that.state.currentChatroom
          });
        }
      });
    }
  }

  DeleteChatroom(chatroomId) {
    let that = this;
    if (chatroomId) {
      ChatroomService.DeleteChatroom(chatroomId).then(chatrooms => {
        that.setState({
          chatrooms: chatrooms,
          currentChatroom:
            chatrooms.length > 0
              ? chatrooms[chatrooms.length - 1].chatroomId
              : 0
        });
      });
    }
  }

  SelectRoom(chatroomId) {
    this.setState({ currentChatroom: chatroomId });
  }

  componentDidMount() {
    let that = this;
    var chatrooms = ChatroomService.getChatrooms().then(chatrooms => {
      if (chatrooms.length) {
        that.setState({
          chatrooms: chatrooms,
          currentChatroom: chatrooms[0].chatroomId
        });
      }
    });
  }
}

export default ChatRoomList;
