import React, { Component } from "react";

export class ChatRoom extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <li className="nav-item">
        <a
          className="nav-link"
          onClick={(e) => {
            e.preventDefault();
            this.props.selectRoom(this.props.chatroom.chatroomId);
          }}
        >
          <span data-feather="home" />
          {this.props.chatroom.title} <span className="sr-only">(current)</span>
          <span id="delete-room"
            onClick={e => {
              e.preventDefault();
              this.props.DeleteChatRoom(this.props.chatroom.chatroomId);
            }}
            aria-hidden="true"
          >
            &times;
          </span>
        </a>
      </li>
    );
  }
}

export default ChatRoom;
