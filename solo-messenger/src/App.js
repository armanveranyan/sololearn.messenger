import React, {Component} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import { PrivateRoute } from './components/PrivateRoute';
import { HomePage } from './HomePage';
import { LoginPage, SignUpPage } from './LoginPage';


class App extends Component {
    render() {
        return (
            <div className="App">
                <Router>
                    <div>
                        <PrivateRoute exact path="/" component={HomePage}/>
                        <Route path="/login" component={LoginPage}/>
                        <Route path="/register" component={SignUpPage}/>
                    </div>
                </Router>
            </div>
        );
    }
}

export default App;
