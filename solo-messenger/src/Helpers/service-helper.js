import {
    UserService
} from "../services/UserService";

export function handleApiResponse(response) {

    return response
        .text()
        .then(text => {

            const data = text && JSON.parse(text);
            console.log(data);
            if (!response.ok) {
                if (response.status === 401) {
                    // auto logout if 401 response returned from api
                    UserService.logout();
                    window.location.reload(true);
                }

                const error = (data && data.message) || response.statusText;
                return Promise.reject(error);
            }

            return data;
        });
}