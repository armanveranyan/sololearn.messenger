import React from "react";
import { Link } from "react-router-dom";
import { NavMenu } from "../components/NavMenu";
import { ChatRoomList } from "../components/ChatRoomList";
import { UserService } from "./../services/UserService";

class HomePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {},
      users: []
    };
  }

  componentDidMount() {
    this.setState({
      user: JSON.parse(localStorage.getItem("user")),
      users: { loading: true }
    });
    UserService.getAll().then(users => this.setState({ users }));
  }

  render() {
    const { user, users } = this.state;
    return (
      <div>
        <NavMenu User={user} />
        <ChatRoomList />
      </div>
    );
  }
}

export { HomePage };
