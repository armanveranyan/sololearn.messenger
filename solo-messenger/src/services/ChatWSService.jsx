import config from "../config";
import { getAuthenticationData } from "../Helpers/auth-header";
import {
  HubConnection,
  HubConnectionBuilder,
  HttpTransportType,
  JsonHubProtocol,
  LogLevel
} from "@aspnet/signalr";

const url = config.apiUrl + "chat";

class ChatWSService {
  Connect(chatroomId, callback) {
    var that = this;

    if (this._connected == true) {
      this._connection
        .invoke("Disconnect", this._chatRoomId)
        .catch(function(err) {
          return console.error(err.toString());
        });

      this._chatRoomId = chatroomId;

      this._connection.invoke("Connect", this._chatRoomId).catch(function(err) {
        return console.error(err.toString());
      });
    } else {
      this._chatRoomId = chatroomId;
      var transport = HttpTransportType.WebSockets;
      var connectionBuilder = new HubConnectionBuilder();
      this._connection = connectionBuilder
        .withUrl(url, {
          accessTokenFactory: () => getAuthenticationData()
        })
        .build();

      var that = this;

      // start connection
      this._connection
        .start()
        .then(() => {
          that._connected = true;
          that._connection
            .invoke("Connect", this._chatRoomId)
            .catch(function(err) {
              return console.error(err.toString());
            });

          // handle on connect
          that._connection.on("Connected", function(username, messages) {
            callback(messages);
          });
        })
        .catch(err => console.error(err, "red"));
    }
  }

  SendMessage(chatroomId, message) {
    if (!this._connected && this._chatRoomId != chatroomId) return;

    this._connection
      .invoke("SendMessage", message, chatroomId)
      .catch(function(err) {
        return console.error(err.toString());
      });
  }

  RegisterCallbacks(ReceivedMessage) {
    this._connection.on("ReceiveMessage", function(user, message) {
      ReceivedMessage(message, user);
    });
  }
  _connection;
  _connected = false;
  _chatRoomId = 0;
}

export const ChatService = new ChatWSService();

export default ChatService;
