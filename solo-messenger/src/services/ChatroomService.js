import config from '../config';

import {
    authHeader
} from '../Helpers/auth-header';

import {
    handleApiResponse
} from '../Helpers/service-helper';

export const ChatroomService = {
    getChatrooms,
    CreateChatroom,
    DeleteChatroom
};

function getChatrooms(username, password) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    return fetch(`${config.apiUrl}chatroom/all`, requestOptions).then(handleApiResponse).then(chatrooms => {
        return chatrooms;
    });

}

function CreateChatroom(chatroomName) {
    const requestOptions = {
        method: 'POST',
        headers: authHeader(),
        body: JSON.stringify({
            chatName: chatroomName
        })
    };

    requestOptions.headers["Content-Type"] = "application/json";

    let that = this;

    return fetch(`${config.apiUrl}chatroom/create`, requestOptions).then(handleApiResponse).then(chatrooms => {
        return chatrooms;
    });
}

function DeleteChatroom(chatroomId) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader(),
    };

    let that = this;

    return fetch(`${config.apiUrl}chatroom/delete/` + chatroomId, requestOptions).then(handleApiResponse).then(chatrooms => {
        return chatrooms;
    });
}

export default ChatroomService;